package com.common.service.security;

public class JwtConfig {
    // Try later because @value require spring
    //    @Value("${security.jwt.uri:/auth/**}")
    private String Uri = "/auth/**";

    //    @Value("${security.jwt.header:Authorization}")
    private String header = "Authorization";

    //    @Value("${security.jwt.prefix:Bearer }")
    private String prefix = "Bearer ";

    //    @Value("${security.jwt.expiration:#{24*60*60}}")
    private int expiration = 24 * 60 * 60;

    //    @Value("${security.jwt.secret:JwtSecretKey}")
    private String secret = "JwtSecretKey";

    public String getUri() {
        return Uri;
    }

    public String getHeader() {
        return header;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getExpiration() {
        return expiration;
    }

    public String getSecret() {
        return secret;
    }

    @Override
    public String toString() {
        return "JwtConfig{" +
                "Uri='" + Uri + '\'' +
                ", header='" + header + '\'' +
                ", prefix='" + prefix + '\'' +
                ", expiration=" + expiration +
                ", secret='" + secret + '\'' +
                '}';
    }

    public void setUri(String uri) {
        Uri = uri;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
