package com.common.service.security;

public enum UserRole {
    USER, ADMIN;
}